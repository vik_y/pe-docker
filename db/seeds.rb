# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Student.transaction do
  (1..100).each do |i|
    Student.create(email: "student#{i}@gmail.com", password: "password")
    Admin.create(email: "admin#{i}@gmail.com", password: "password")
  end
end


# Create some projects
Project.transaction do
  (1..10).each do |i|
    Project.create(name: "Project#{i}", admin: Admin.find_by_email("admin#{i}@gmail.com"), start_time: DateTime.now, end_time: DateTime.now+20.days)
  end

  # Add some students to all the projects
  Project.all.each do |p|
    p.add_students(Student.first(10))
  end

  Admin.first(10).each do |a|
    (1..10).each do |i|
      Project.create(name: "Admin#{a.id}_Project#{i}", admin: a, start_time: DateTime.now, end_time: DateTime.now+20.days)
    end

  end
end


