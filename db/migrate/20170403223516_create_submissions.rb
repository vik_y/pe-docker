class CreateSubmissions < ActiveRecord::Migration[5.0]
  def change
    create_table :submissions do |t|
      t.integer :project_id
      t.integer :student_id
      t.string :comments

      t.timestamps
    end
  end
end
