class StudentProjectRelation < ActiveRecord::Migration[5.0]
  def change
    create_table :students_projects, id: false do |t|
      t.belongs_to :student, index: true
      t.belongs_to :project, index: true
    end
  end
end
