class Submission < ApplicationRecord
  BASE_URL = 'http://localhost:5000'
  belongs_to :student
  belongs_to :project

  validate :student
  validate :project

  # TODO: Add validations and hooks

  validates_uniqueness_of :student_id, :scope => :project_id

  before_create :generate_tag

  # Connects with docker repository to find the image.
  def verify
    # TODO: See if there is a better way to do this.
    response = HTTParty.get(BASE_URL + "/v2/_catalog")
    puts response.body

    json = JSON.parse(response.body)

    r = json['repositories']

    if r.include? self.image_tag
      self.status = SUBMISSION_VERIFIED
      return self.save
    else
      errors.add(:base, "Image hasn't been uploaded yet.")
      return false
    end
  end

  # Generate tag to be used by students for submission
  def generate_tag
    # TODO: Add a random 4 letter text here
    self.image_tag = "#{self.project.id.to_s}/#{self.student.id.to_s}"
  end
end
