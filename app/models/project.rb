class Project < ApplicationRecord
  belongs_to :admin
  has_many :submissions

  # Validations
  validate :admin
  validates_presence_of :name

  before_create :validate_dates


  # TODO: Add validations and hooks

  def validate_dates
    puts "Start", start_time
    puts "End", end_time
    if start_time > end_time
      errors.add(:base, "End time can't be earlier than start time")
      return false
    end
  end

  # Add submission for multiple students
  def add_students students
    students.each do |s|
      add_student s
    end
  end

  # Add a submission for a student for this project
  def add_student student
    s = Submission.new(project: self, student: student)
    self.submissions << s
  end
end
