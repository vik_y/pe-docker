class StudentsController < ApplicationController
  layout 'dashboard'
  before_action :check_admin
  before_action :authenticate_student!
  def index
    student = current_student
    @submissions = student.submissions
  end

  # Temporary workaround to fix the redirection issue after login
  def check_admin
    if !current_admin.nil?
      redirect_to admins_path
      return
    end
  end
end
