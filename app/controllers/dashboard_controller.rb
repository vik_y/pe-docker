class DashboardController < ApplicationController
  layout 'dashboard'
  def index

  end

  def student
    authenticate_student!
    student = current_student
    @submissions = student.submissions
  end

  def admin
    authenticate_admin!
    @projects = current_admin.projects
  end
end
