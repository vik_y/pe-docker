class AdminsController < ApplicationController
  layout 'dashboard'
  before_action :authenticate_admin!
  def index
    @projects = current_admin.projects
  end
end
