json.extract! project, :id, :admin_id, :name, :start_time, :end_time, :created_at, :updated_at
json.url project_url(project, format: :json)
