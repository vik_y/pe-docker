json.extract! submission, :id, :project_id, :student_id, :comments, :created_at, :updated_at
json.url submission_url(submission, format: :json)
