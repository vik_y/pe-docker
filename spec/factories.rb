# This will guess the User class
FactoryGirl.define do
  factory :user do
    email {Faker::Internet.email}
    password {"thanks123"}
  end

  factory :admin do
    email {Faker::Internet.email}
    password {"thanks123"}
  end

  factory :student do
    email {Faker::Internet.email}
    password {"thanks123"}
  end

  factory :project do
    admin
    name {"Random"}
    start_time {DateTime.now}
    end_time {DateTime.now+20.days}
  end

  factory :submission do
    project
    student
  end
end