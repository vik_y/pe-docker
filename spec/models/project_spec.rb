require 'rails_helper'


# TODO: Put the testing suite in place.

RSpec.describe Project, type: :model do
  it "has a valid factory" do
    p = create(:project)
  end

  it "cannot have blank name" do
    p = build(:project, :name=> "")
    expect(p.save).to eq(false)
  end

  it "cannot have end_date earlier than or equal start date" do
    p = build(:project, :start_time=>DateTime.now, :end_time=>DateTime.now+20.days)
    expect(p.save).to eq(true)

    p = build(:project, :start_time=>DateTime.now, :end_time=>DateTime.now-20.days)
    expect(p.save).to eq(false)

    t = DateTime.now
    p = build(:project, :start_time=>t, :end_time=>t)
    expect(p.save).to eq(false)
  end

  it "can add submission" do
    student = create(:student)
    project = create(:project)
    project.add_student(student)

    expect(project.submissions.count).to eq(1)
  end

  it "cannot add submission for same student twice" do
    student = create(:student)
    project = create(:project)
    project.add_student(student)

    expect(project.submissions.count).to eq(1)
    expect(project.add_student(student)).to eq(false)
    expect(project.submissions.count).to eq(1)
  end
end
