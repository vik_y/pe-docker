require 'rails_helper'

RSpec.describe Submission, type: :model do
  it "has a valid factory" do
    s = create(:submission)
  end

  it "can be created only for those registered for the project"

  it "generates image_tag after create" do
    s = create(:submission)
    expect(s.image_tag).to eq("#{s.project.id.to_s}/#{s.student.id.to_s}")
  end
end
