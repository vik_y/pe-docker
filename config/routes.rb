Rails.application.routes.draw do
  resources :submissions
  get 'submissions/:id/verify' => 'submissions#verify'
  resources :projects
  post 'projects/import' => 'projects#import'

  devise_for :users
  devise_for :admins
  devise_for :students

  resources :admins, only: [:index]
  resources :students, only: [:index]

  #get 'dashboard' => 'dashboard#index', as: "user_root_path"
  #get 'dashboard/admin' => 'dashboard#admin', as: "admin_root_path"
  #get 'dashboard/student' => 'dashboard#student', as: "student_root_path"
  root to: 'students#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
