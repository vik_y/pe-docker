[![Build Status](https://travis-ci.com/vik-y/pe.svg?token=XmjqYLjMqSiFxvjpmA2x&branch=master)](https://travis-ci.com/vik-y/pe)

# README

Installation Instructions: 

First you have to make sure docker is installed on your system. If not installed already then you can install docker using this method: 
```sh
curl -sSL https://get.docker.com/ | sh

# Replace <your_user> with your username 
sudo usermod -aG docker <your_user>
```
NOTE: Docker installation is done. You have to log out and in log to start using docker.

After logging back in check if the following command is running successfully or not

docker run hello-world
If it does then your docker installation is done.

Now install all the required dependencies:
```sh
sudo apt-get install git python-pip 
sudo pip install requests==2.6.0
sudo pip install docker-compose
```
Now for the first run of the program copy paste the following commands: 
```sh

cd pe-docker
docker-compose run web bundle exec rake db:create
docker-compose run web bundle exec rake db:migrate
docker-compose up 
```

Congratulations, installation is done. Now you can access the development app at localhost:3000 

Note that every time you stop the app you can restart it using `docker-compose up`